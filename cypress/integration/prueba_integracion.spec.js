describe('Pruebas de integración continua con Cypress', function(){
  
  it('Navegación dentro de la plataforma EDUC', function(){
      cy.visit('https://educ.ucol.mx/')
  })
  
  it('Inicio de sesion y navegación en EDUC', function(){
    // Se inicia en EDUC seleccionando la opcion de estudiantes
    cy.get('.login-buttons').click()
      cy.get('.l-idp > p > input').first().click({multiple: true})
      
    //   Se ingresan las llaves de acceso para ingresar a EDUC con cuenta de universitario
      cy.get('#username').type('20136208').should('have.value', '20136208')
      cy.get('#password').type('marioa98').should('have.value', 'marioa98')
      cy.get('.btn').click()

      cy.wait(2000) //Espera 2 segundos a que terminen de cargar los elementos de la pagina
      cy.get('.course-item-details > h5 > a').first().click({multiple: true})

  })
})